/**
 * This library is wrapper arround d3js and provide basic functionality
 */
(function (window) {
    // creating global variable which will be return on load of this library
    if (!window.d3) {
        alert("Please include d3 library version 3.5.17 before loading this library");
        return;
    }
    var jujhar = {};
    window.jujhar = jujhar;
    jujhar.sayHello = function (name) {
        console.log("Hello!" + name);
    };

    /**
     * 
     * @param {*} options = {
     *                          data:dataArray
     *                          dataFilePath : dataFilePath
     *                          dataFileFormat: datafileformat
     *                          domID: dom id
     *                          yLable : yAxis lable
     *                          xParam: xAxis parameter in provided data
     *                          yParam: yAxis parameter in provided data
     * }
     * 
     */
    jujhar.generateBarGraph = function (options) {
        if (options.data === undefined && (options.dataFilePath === undefined || options.dataFilePath.length === 0)) {
            throw new Error("Either provide data array or provide file path of data and format for data {data:[]} and for datafile {dataFilePath:\"filePath\"}");
        }
        if (options.data === undefined && options.dataFileFormat === undefined) {
            new Error("Since you are providing dataFilePath then you need to provide dataFileFormat as well");
        }
        if (options.domID === undefined || options.domID.length === 0) {
            throw new Error("Please provide domID where you need to generate bar graph");
        }
        if (options.xParam === undefined || options.xParam.length === 0) {
            throw new Error("Please provide xParam which defines name of data property which you want to use as xAxis");
        }
        if (options.yParam === undefined || options.yParam.length === 0) {
            throw new Error("Please provide yParam which defines name of data property which you want to use as yAxis");
        }
        if (options.data === undefined) {
            if (options.dataFileFormat === "csv") {
                d3.csv(options.dataFilePath, function (data) {
                    options.data = data;
                    callBack();
                });
            }
            else if (options.dataFileFormat === "json" || options.dataFileFormat === "geojson") {
                d3.json(options.dataFilePath, function (data) {
                    options.data = data;
                    callBack();
                });
            }
        }
        function callBack() {
            if (options.data.length === 0) {
                throw new Error("User provided no data, please check provided dataFile or provide dataArray");
            }
            if (options.data[0][options.xParam] === undefined) {
                throw new Error("Provided xParam doesn't match with provided data");
            }
            if (options.data[0][options.yParam] === undefined) {
                throw new Error("Provided yParam doesn't match with provided data");
            }
            if (options.yLabel === undefined) {
                throw new Error("Provide yAxis lable to show unit of data");
            }
            var margin = { top: 20, right: 10, bottom: 100, left: 50 },
                width = 700 - margin.right - margin.left,
                height = 500 - margin.top - margin.bottom;
            if (options.domID[0] !== "#") {
                options.domID = "#" + options.domID;
            }
            var svg = d3.select(options.domID)
                .append("svg")
                .attr({
                    "width": width + margin.right + margin.left,
                    "height": height + margin.top + margin.bottom
                })
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.right + ")");
            var xScale = d3.scale.ordinal()
                .rangeRoundBands([0, width], 0.2, 0.2);
            var yScale = d3.scale.linear().range([height, 0]);
            // define x axis and y axis
            var xAxis = d3.svg.axis()
                .scale(xScale)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(yScale)
                .orient("left");

            // to make sure that yAxis data is integer 
            options.data.forEach(function (d) {
                d[options.xParam] = d[options.xParam];
                d[options.yParam] = +d[options.yParam];
            });

            // sort data so that it makes easier to understand bar graph
            options.data.sort(function (a, b) {
                return b[options.yParam] - a[options.yParam];
            });

            // add domain to scales
            xScale.domain(options.data.map(function (d) { return d[options.xParam]; }));
            yScale.domain([0, d3.max(options.data, function (d) { return d[options.yParam]; })]);
            //yScale.domain([0, options.data[0][options.yParam]]);



            var color = "steelblue";
            var colorScale = d3.scale.linear()
                .domain([options.data[options.data.length - 1][options.yParam], options.data[0][options.yParam]])
                .range([d3.rgb(color).brighter(), d3.rgb(color).darker()]);

            
                                                        
            svg.selectAll('rect').data(options.data)
                .enter()
                .append('rect')
                // .attr("height", 0)
                // .attr("y", height)
                // .transition().duration(3000)
                // .delay(function (d, i) { return i * 200; })
                .attr({
                    "x": function (d) { return xScale(d[options.xParam]); },
                    "y": function (d) { return yScale(d[options.yParam]); },
                    "width": xScale.rangeBand(),
                    "height": function (d) { return height - yScale(d[options.yParam]); },

                    "fill": function (d) { return colorScale(d[options.yParam]); }
                })
                .on("mouseover",function(d){
                    tooltip.style("display","block");
                    // var xPos = d3.mouse(this)[0]-15;
                    // var yPos = d3.mouse(this)[1]-25;

                    // tooltip.select('text')
                    //             .text(function(d){ return options.xParam+":"+d[options.xParam]+","+options.yParam+":"+d[options.yParam]; })
                    //             .attr({
                    //                 "transform":"translate("+xPos+","+yPos+")"
                    //             });
                })
                .on("mouseout",function(d){
                    tooltip.style("display","none");
                })
                .on("mousemove",function(d){
                    tooltip.style("display","block");
                    var xPos = d3.mouse(this)[0]-55;
                    var yPos = d3.mouse(this)[1]-25;

                    tooltip.select('text')
                                .text(options.xParam+":"+d[options.xParam]+","+options.yParam+":"+d[options.yParam])
                                .attr({
                                    "transform":"translate("+xPos+","+yPos+")"
                                });
                });
                //.style("fill", function (d, i) { return 'rgb(20, 20, ' + ((i * 30) + 100) + ')' });

            svg.selectAll('text').data(options.data)
                .enter()
                .append('text')
                .attr({
                    "x": function (d) { return xScale(d[options.xParam]) + xScale.rangeBand() / 2; },
                    "y": function (d) { return yScale(d[options.yParam]) + 12; },
                    "font-family": 'sans-serif',
                    "font-size": '13px',
                    "font-weight": 'bold',
                    "fill": 'white',
                    "text-anchor": 'middle'
                })
                .text(function (d) { return d[options.yParam]; });

            // xAxis with lable
            svg.append('g')
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll('text')
                .attr("dx", "-.8em")
                .attr("dy", ".25em")
                .attr("transform", "rotate(-60)")
                .style("text-anchor", "end")
                .attr("font-size", "10px");

            // yAxis with label

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("x", -height / 2)
                .attr("dy", "-3em")
                .style("text-anchor", "middle")
                .text(options.yLabel);

            var tooltip = svg.append('g')
                                .attr("class","tooltip")
                                .attr("display","none");
            tooltip.append("text")
                        .attr({
                            "font-size":"1em",
                            "font-weight":"bold"
                            
                        });

        }






    }
    return jujhar;

})(window);